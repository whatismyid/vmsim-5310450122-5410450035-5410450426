----------< Arguments >-------------
Trace file name         : trace1.txt
Replacement algorithm   : fifo
Page size               : 1024 bytes
Replacement policy      : local
------------------------------------
------< Trace Information >---------
Total processes         : 4
Total disk reads        : 3951
Total disk writes       : 0
------------------------------------
-----------< Results >--------------
Total page faults       : 3951
Total disk reference    : 3951
Page fault rate         : 3.95
------------------------------------
------------------------------------
------------------------------------
----------< Arguments >-------------
Trace file name         : trace1.txt
Replacement algorithm   : fifo
Page size               : 256 bytes
Replacement policy      : local
------------------------------------
------< Trace Information >---------
Total processes         : 4
Total disk reads        : 5388
Total disk writes       : 0
------------------------------------
-----------< Results >--------------
Total page faults       : 5388
Total disk reference    : 5388
Page fault rate         : 5.39
------------------------------------
------------------------------------
------------------------------------
----------< Arguments >-------------
Trace file name         : trace1.txt
Replacement algorithm   : fifo
Page size               : 256 bytes
Replacement policy      : local
------------------------------------
------< Trace Information >---------
Total processes         : 4
Total disk reads        : 5388
Total disk writes       : 0
------------------------------------
-----------< Results >--------------
Total page faults       : 5388
Total disk reference    : 5388
Page fault rate         : 5.39
------------------------------------
------------------------------------
------------------------------------
